# Team Information
<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-3-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

Welcome to the Debian Bazel Build System Maintainers Team! Below is some information summarizing the team, our goal, and our plans. We are currently supporting the Science and Med teams in finishing Phase 1 of our plan while simultaneously starting on Phase 2.

The team is currently using a [mailing list on Launchpad](mailto:bazel-team@lists.launchpad.net) for package tracking and our [official Debian mailing list](mailto:debian-bazel@lists.debian.org) for all discussions.

## Goal

Package and maintain Bazel as a first-class citizen build system in Debian

## Plans

### 1. [Short-Term](https://salsa.debian.org/bazel-team/meta/-/wikis/Workplan-Part-1) -- Bazel for TensorFlow

Minimum effort required to get TensorFlow in the Debian main archive. This was the initial goal from the COVID-19 Biohackathon.

### 2. [Mid-Term](https://salsa.debian.org/bazel-team/meta/-/wikis/Workplan-Part-2) -- Bazel for End Users

Produce a Bazel package so that users can build Bazel-based codebases for their own development purposes (not for Debian packaging). Downloads from the Internet are explicitly allowed.

### 3. [Long-Term](https://salsa.debian.org/bazel-team/meta/-/wikis/Workplan-Part-3) -- Bazel for Debian Packaging

Produce a Bazel package that is a first-class build system from the Debian/DebHelper perspective. No hacks are needed, every dependency in a Bazel build file is satisfiable offline if there is a Debian package for it.

## [Source Package Summary](https://tracker.debian.org/teams/bazel/)

1.  [**bazel-bootstrap**](https://salsa.debian.org/bazel-team/bazel-bootstrap) (status: available in bullseye, bookworm, and sid)

    1.1.  Built from `bazel-*-dist` upstream package but modified to remove unnecessary components

    1.2.  Includes archives of dependencies which require Bazel to build (packages 3-10 below)
    
    1.3.  Includes source code for some third-party packages that are not available in Debian

    1.4.  Upstream source location: https://github.com/bazelbuild/bazel/releases/download/3.5.1/bazel-3.5.1-dist.zip (requires `Files-Excluded` in `debian/copyright`)

    1.5.  Versions >= 3.5.1+ds-4 build the bazel-bootstrap-source "binary" package which is required to build some Bazel components.

    1.6.  Build Dependencies: libchecker-framework-java, liberror-prone-java, libescapevelocity-java, libgoogle-api-client-java, libgoogle-auth-java, libgoogle-auto-common-java, libgoogle-auto-service-java, libgoogle-auto-value-java, libgoogle-flogger-java, libgoogle-gson-java, libgoogle-http-client-java, libgrpc-java, libgrpc++-dev, libguava-java, libopencensus-java, libperfmark-java, libprotobuf-dev, libprotobuf-java, libprotoc-dev, libtruth-java, openjdk-11-jdk-headless, protobuf-compiler, protobuf-compiler-grpc, protobuf-compiler-grpc-java-plugin, velocity, other generic Debian packages.

2.  [**bazel-*X***](https://salsa.debian.org/bazel-team/bazel) (status: will adapt bazel-bootstrap packaging once packages 3-10 are complete)

    2.1.  Built from upstream release Git tag pull

    2.2.  No archived dependencies
    
    2.3.  Includes source code for some third-party packages that are not available in Debian (these may be separately packaged in the future on a case-by-case basis)

    2.4.  Uses [`--override_repository`](https://docs.bazel.build/versions/master/command-line-reference.html#flag--override_repository) to reference dependency source location instead of using pre-packaged archives

    2.5.  Must have an existing Bazel binary to build (provided by either `bazel-bootstrap` or `bazel`)
    
    2.6.  Will build a major-version binary package (e.g. `bazel-3`) as well as a dependency package (`bazel-latest`). Only the newest Debian `bazel-*X*` source package will build the `bazel-latest` dependency package. The `bazel-*X*` binary packages will be co-installable to allow reasonable transitions for projects built with Bazel, but they will not be co-installable with `bazel-bootstrap`.

    2.7.  Upstream source location: https://github.com/bazelbuild/bazel/archive/3.5.1.tar.gz (requires `Files-Excluded` in `debian/copyright`)

    2.8.  Build Dependencies: **bazel-java-tools, bazel-platforms, bazel-rules-cc, bazel-rules-java, bazel-rules-pkg, bazel-rules-proto, bazel-stardoc, bazel-skylib**, libchecker-framework-java, liberror-prone-java, libescapevelocity-java, libgoogle-api-client-java, libgoogle-auth-java, libgoogle-auto-common-java, libgoogle-auto-service-java, libgoogle-auto-value-java, libgoogle-flogger-java, libgoogle-gson-java, libgoogle-http-client-java, libgrpc-java, libgrpc++-dev, libguava-java, libopencensus-java, libperfmark-java, libprotobuf-dev, libprotobuf-java, libprotoc-dev, libtruth-java, openjdk-11-jdk-headless, protobuf-compiler, protobuf-compiler-grpc, protobuf-compiler-grpc-java-plugin, velocity, other generic Debian packages.

3.  [**bazel-java-tools**](https://salsa.debian.org/bazel-team/bazel-java-tools) (status: embedded in bazel-bootstrap, needs separate packaging for Phase 2)

    3.1.  Upstream source location: https://github.com/bazelbuild/java_tools

    3.2.  Build dependencies: ???

4.  [**bazel-platforms**](https://salsa.debian.org/bazel-team/bazel-platforms) (status: embedded in bazel-bootstrap, separate packaging for Phase 2 in bookworm and sid)

    4.1.  Upstream source location: https://github.com/bazelbuild/platforms

    4.2.  Build dependencies: None

5.  [**bazel-rules-cc**](https://salsa.debian.org/bazel-team/bazel-rules-cc) (status: embedded in bazel-bootstrap, needs separate packaging for Phase 2)

    5.1.  Upstream source location: https://github.com/bazelbuild/rules_cc

    5.2.  Build dependencies: None?

6.  [**bazel-rules-java**](https://salsa.debian.org/bazel-team/bazel-rules-java) (status: embedded in bazel-bootstrap, separate packaging for Phase 2 in bookworm and sid)

    6.1.  Upstream source location: https://github.com/bazelbuild/rules_java

    6.2.  Build dependencies: None

7.  [**bazel-rules-pkg**](https://salsa.debian.org/bazel-team/bazel-rules-pkg) (status: embedded in bazel-bootstrap, needs separate packaging for Phase 2)

    7.1.  Upstream source location: https://github.com/bazelbuild/rules_pkg

    7.2.  Build dependencies: None?

8.  [**bazel-rules-proto**](https://salsa.debian.org/bazel-team/bazel-rules-proto) (status: embedded in bazel-bootstrap, needs separate packaging for Phase 2)

    8.1.  Upstream source location: https://github.com/bazelbuild/rules_proto

    8.2.  Build dependencies: None?

9.  [**bazel-stardoc**](https://salsa.debian.org/bazel-team/bazel-stardoc) (status: embedded in bazel-bootstrap, needs separate packaging for Phase 2)

    9.1.  Upstream source location: https://github.com/bazelbuild/stardoc

    9.2.  Build dependencies: **bazel|bazel-bootstrap, bazel-source|bazel-bootstrap-source, bazel-rules-java, bazel-skylib**

10.  [**bazel-skylib**](https://salsa.debian.org/bazel-team/bazel-skylib) (status: embedded in bazel-bootstrap, separate packaging for Phase 2 in bookworm and sid)

     10.1.  Upstream source location: https://github.com/bazelbuild/bazel-skylib

     10.2.  Build dependencies: None

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://salsa.debian.org/olek"><img src="https://seccdn.libravatar.org/avatar/b16db0e2352cc209e025c671afa3ec80?s=80&d=identicon" width="100px;" alt=""/><br /><sub><b>Olek Wojnar</b></sub></a><br /><a href="https://salsa.debian.org/bazel-team/meta/commits/master" title="Documentation">📖</a> <a href="#platform-olek" title="Packaging/porting to new platform">📦</a></td>
    <td align="center"><a href="https://salsa.debian.org/crusoe"><img src="https://seccdn.libravatar.org/avatar/e9ff07597f1bc11c339b81b5bca05830?s=80&d=identicon" width="100px;" alt=""/><br /><sub><b>Michael R. Crusoe</b></sub></a><br /><a href="#ideas-crusoe" title="Ideas, Planning, & Feedback">🤔</a> <a href="#projectManagement-crusoe" title="Project Management">📆</a></td>
    <td align="center"><a href="https://salsa.debian.org/pcloudy-guest"><img src="https://seccdn.libravatar.org/avatar/525278bf2af701cff6e3cae679e342e8?s=80&d=identicon" width="100px;" alt=""/><br /><sub><b>Yun Peng</b></sub></a><br /><a href="#platform-pcloudy-guest" title="Packaging/porting to new platform">📦</a></td>
  </tr>
</table>

<!-- markdownlint-enable -->
<!-- prettier-ignore-end -->
<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
